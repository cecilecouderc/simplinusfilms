<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RolesRepository")
 */
class Roles
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $slug;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\RolesUsers", mappedBy="role")
     */
    private $rolesUsers;

    public function __construct()
    {
        $this->rolesUsers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @return Collection|RolesUsers[]
     */
    public function getRolesUsers(): Collection
    {
        return $this->rolesUsers;
    }

    public function addRolesUser(RolesUsers $rolesUser): self
    {
        if (!$this->rolesUsers->contains($rolesUser)) {
            $this->rolesUsers[] = $rolesUser;
            $rolesUser->setRole($this);
        }

        return $this;
    }

    public function removeRolesUser(RolesUsers $rolesUser): self
    {
        if ($this->rolesUsers->contains($rolesUser)) {
            $this->rolesUsers->removeElement($rolesUser);
            // set the owning side to null (unless already changed)
            if ($rolesUser->getRole() === $this) {
                $rolesUser->setRole(null);
            }
        }

        return $this;
    }

    public function __toString(){
        return $this->name;
    }
}
