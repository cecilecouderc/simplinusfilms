<?php

namespace App\Controller;

use App\Entity\Movies;
use App\Entity\Orders;
use App\Entity\MoviesOrders;


use App\Form\MovieType;
use App\Form\MovieEditType;

use App\Entity\MovieSearch;
use App\Form\MovieSearchType;
use App\Repository\MoviesRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\FormBuilderInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class MoviesController extends AbstractController
{
    //lister la totalité des films
    /**
     * @Route("/home", name="home")
     */
    public function index(Request $request)
    {
        //-- lister totalité des films

        $repo = $this->getDoctrine()->getRepository(Movies::class);
        $movies = $repo->findAll();


        //-- récupérer les commandes de l'utilisateur connecté

        $iduser = $this->getUser()->getId();
        $repo2 = $this->getDoctrine()->getRepository(Orders::class);
        $userorders = $repo2->findBy(
            ['user_id' => $iduser]

        );

        // dd($userorders);

        //-- Pour chacune de ces commandes, on se débrouille pour avoir un tableau de films (Cf $this->films)

        foreach($userorders as &$userorder)
        {
            $userorder->films = $userorder->getMoviesOrders()->toArray();
        }
        
        //dd($userorders);

        $search = new MovieSearch();
        $form = $this->createForm(MovieSearchType::class, $search);
        $form->handleRequest($request);
        //dd($search);

        if ($form->isSubmitted() && $form->isValid()) {
            // $search = $request->query->get('Moviesearch');

            $movies = $repo->searchOccurenceInTitleAndResume($search->getMoviesearch());
        }

        return $this->render('movies/index.html.twig', [
            'movies' => $movies,
            'form' => $form->createView(),
            'userorders' => $userorders
        ]);
    }

    // Ajouter un film à la liste by ADMIN
    /**
     * @Route("/add", name="add")
     * @IsGranted("ROLE_ADMIN")
     */
    public function add(Request $request, ObjectManager $manager)
    {
        $movie = new Movies();
        $form = $this->createForm(MovieType::class, $movie);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $manager->persist($movie);
            $manager->flush();
        }

        return $this->render('movies/add_movie.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    //Modifier un film SAUF titre by ADMIN
    /**
     * @Route("admin/edit/{id}", name="edit")
     */
    public function edit($id, Request $request, Movies $movie, EntityManagerInterface $em)
    {
        $repository = $this->getDoctrine()->getRepository(Movies::class);
        $movies = $repository->find($id);
       
              // $movies = $em->getRepository(Movies::class)->find($id);
              
        $form = $this->createForm(MovieEditType::class, $movie);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
        }
        return $this->render('movies/edit_movie.html.twig', [
            'movies' => $movies,
            'form' => $form->createView(),
        ]);
    }
    //Supprimer un film by ADMIN
    /**
     * @Route("/admin/remove/{id}", name="remove")
     */
    public function remove($id, ObjectManager $entityManager)
    {
        $moviesRepo = $entityManager->getRepository(Movies::class);
        $identifiant = $moviesRepo->find($id);
        $entityManager->remove($identifiant);
        $entityManager->flush($identifiant);

        return $this->redirectToRoute('home');

        // On vérifie : 
        // $identifiant= $moviesRepo->find($id);
        // var_dump($identifiant);
    }

    //Filtrer par titre by USER & ADMIN
    /**
     * @Route("/filterbytitle", name="filterbytitle")
     */
    //filter/classer les films 
    public function filterbytitle(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository(Movies::class);
        $movies = $repository->filterbytitle();

        $search = new MovieSearch();
        $form = $this->createForm(MovieSearchType::class, $search);
        $form->handleRequest($request);

        $search = new MovieSearch();
        $form = $this->createForm(MovieSearchType::class, $search);
        $form->handleRequest($request);



        //-- récupérer les commandes de l'utilisateur connecté

        $iduser = $this->getUser()->getId();
        $repo2 = $this->getDoctrine()->getRepository(Orders::class);
        $userorders = $repo2->findBy(
            ['user_id' => $iduser]

        );

        // dd($userorders);

        //-- Pour chacune de ces commandes, on se débrouille pour avoir un tableau de films (Cf $this->films)

        foreach($userorders as &$userorder)
        {
            $userorder->films = $userorder->getMoviesOrders()->toArray();
        }

        return $this->render('movies/index.html.twig', [
            'movies' => $movies,
            'form' => $form->createView(),
            'userorders' => $userorders
        ]);
    }

    //Filtrer par date by USER & ADMIN
    /**
     * @Route("/filterbyreleaseddate", name="filterbyreleaseddate")
     */
    //filter/classer les films 
    public function filterbyreleaseddate(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository(Movies::class);
        $movies = $repository->filterbyreleaseddate();

        $search = new MovieSearch();
        $form = $this->createForm(MovieSearchType::class, $search);
        $form->handleRequest($request);

        //-- récupérer les commandes de l'utilisateur connecté

        $iduser = $this->getUser()->getId();
        $repo2 = $this->getDoctrine()->getRepository(Orders::class);
        $userorders = $repo2->findBy(
            ['user_id' => $iduser]

        );

        // dd($userorders);

        //-- Pour chacune de ces commandes, on se débrouille pour avoir un tableau de films (Cf $this->films)

        foreach($userorders as &$userorder)
        {
            $userorder->films = $userorder->getMoviesOrders()->toArray();
        }

        return $this->render('movies/index.html.twig', [
            'movies' => $movies,
            'form' => $form->createView(),
            'userorders' => $userorders

        ]);
    }
    //Filtrer par disponibilité by USER & ADMIN
    /**
     * @Route("/filteravailablefirst", name="filteravailablefirst")
     */
    //filter/classer les films 
    public function filteravailablefirst(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository(Movies::class);
        $movies = $repository->filteravailablefirst();

        $search = new MovieSearch();
        $form = $this->createForm(MovieSearchType::class, $search);
        $form->handleRequest($request);

        //-- récupérer les commandes de l'utilisateur connecté

        $iduser = $this->getUser()->getId();
        $repo2 = $this->getDoctrine()->getRepository(Orders::class);
        $userorders = $repo2->findBy(
            ['user_id' => $iduser]

        );

        // dd($userorders);

        //-- Pour chacune de ces commandes, on se débrouille pour avoir un tableau de films (Cf $this->films)

        foreach($userorders as &$userorder)
        {
            $userorder->films = $userorder->getMoviesOrders()->toArray();
        }

        return $this->render('movies/index.html.twig', [
            'movies' => $movies,
            'form' => $form->createView(),
            'userorders' => $userorders

        ]);
    }

    //Filtrer par durée by USER & ADMIN
    /**
     * @Route("/filterbyruntime", name="filterbyruntime")
     */
    //filter/classer les films 
    public function filterbyruntime(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository(Movies::class);
        $movies = $repository->filterbyruntime();

        $search = new MovieSearch();
        $form = $this->createForm(MovieSearchType::class, $search);
        $form->handleRequest($request);

        //-- récupérer les commandes de l'utilisateur connecté

        $iduser = $this->getUser()->getId();
        $repo2 = $this->getDoctrine()->getRepository(Orders::class);
        $userorders = $repo2->findBy(
            ['user_id' => $iduser]

        );

        // dd($userorders);

        //-- Pour chacune de ces commandes, on se débrouille pour avoir un tableau de films (Cf $this->films)

        foreach($userorders as &$userorder)
        {
            $userorder->films = $userorder->getMoviesOrders()->toArray();
        }

        return $this->render('movies/index.html.twig', [
            'movies' => $movies,
            'form' => $form->createView(),
            'userorders' => $userorders

        ]);
    }
}
