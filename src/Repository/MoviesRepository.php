<?php

namespace App\Repository;

use App\Entity\Movies;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Movies|null find($id, $lockMode = null, $lockVersion = null)
 * @method Movies|null findOneBy(array $criteria, array $orderBy = null)
 * @method Movies[]    findAll()
 * @method Movies[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MoviesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Movies::class);
    }
    
    
    public function filterbytitle()
    {
        return $this->createQueryBuilder('m')
            ->orderBy('m.title', 'ASC')
          
            ->getQuery()
            ->getResult()
        ;
    }

    
    public function filterbyreleaseddate()
    {
        return $this->createQueryBuilder('m')
            ->orderBy('m.year', 'DESC')
          
            ->getQuery()
            ->getResult()
        ;
    }

    public function filteravailablefirst()
    {
        return $this->createQueryBuilder('m')
            ->orderBy('m.available', 'DESC')
          
            ->getQuery()
            ->getResult()
        ;
    }

    public function filterbyruntime()
    {
        return $this->createQueryBuilder('m')
            ->orderBy('m.runtime', 'ASC')
          
            ->getQuery()
            ->getResult()
        ;
    }

    public function searchOccurenceInTitleAndResume($user_request)
    {
        $qb= $this->createQueryBuilder('m');
            $qb->where(
                $qb->expr()->orX(
                    $qb->expr()->like('m.title',':user_request'),
                    $qb->expr()->like('m.resume',':user_request')
                )
            )
            ->setParameter('user_request', '%' .$user_request.'%' )
            ->orderBy('m.title', 'ASC');
          
         return $qb   
            ->getQuery()
            ->getResult()
        ;
    }
    

    // /**
    //  * @return Movies[] Returns an array of Movies objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Movies
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
