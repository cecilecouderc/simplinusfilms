<?php

namespace App\Repository;

use App\Entity\MoviesSearch;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method MoviesSearch|null find($id, $lockMode = null, $lockVersion = null)
 * @method MoviesSearch|null findOneBy(array $criteria, array $orderBy = null)
 * @method MoviesSearch[]    findAll()
 * @method MoviesSearch[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MoviesSearchRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MoviesSearch::class);
    }

    // /**
    //  * @return MoviesSearch[] Returns an array of MoviesSearch objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MoviesSearch
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
